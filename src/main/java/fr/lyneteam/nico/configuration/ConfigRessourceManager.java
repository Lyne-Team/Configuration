package fr.lyneteam.nico.configuration;

public interface ConfigRessourceManager {
	public byte[] getConfigRessource(String name) throws Exception;
}