package fr.lyneteam.nico.configuration;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import fr.lyneteam.nico.configuration.file.YamlConfiguration;

public class Config extends YamlConfiguration {
	private final File file;
	private YamlConfiguration save;
	
	public Config(File file) throws Exception {
		this.file = file;
		if (file.exists() || file.isFile()) this.load(file);
	}
	
	public Config(ConfigRessourceManager manager, File file) throws Exception {
		this(file);
		this.saveDefaultConfig(manager);
	}
	
	public final File getFile() {
		return this.file;
	}
	
	public void reload() {
		try {
			this.load(this.file);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public void save() {
		try {
			if (!this.file.exists()) this.file.createNewFile();
			this.save(this.file);
		} catch (IOException exception) {
			
		}
	}
	
	public void saveDefaultConfig(ConfigRessourceManager manager) throws Exception {
		if (!(manager == null)) {
			if (!this.file.exists() || this.file.isDirectory()) {
				this.file.createNewFile();
				DataOutputStream writer = new DataOutputStream(new FileOutputStream(file));
				writer.write(manager.getConfigRessource(this.file.getName()));
				writer.flush();
				writer.close();
			}
			File backup = new File(this.file.getParent(), this.file.getName() + ".back");
			DataOutputStream writer = new DataOutputStream(new FileOutputStream(backup));
			writer.write(manager.getConfigRessource(this.file.getName()));
			writer.flush();
			writer.close();
			this.save = new YamlConfiguration();
			this.save.load(backup);
		}
	}
	
	@Override
	public boolean getBoolean(String path) {
		if (this.contains(path) && this.isBoolean(path)) return super.getBoolean(path);
		if (this.save == null) return false;
		return this.save.getBoolean(path);
	}
	
	@Override
	public int getInt(String path) {
		if (this.contains(path) && this.isInt(path)) return super.getInt(path);
		if (this.save == null) return 0;
		return this.save.getInt(path);
	}
	
	@Override
	public String getString(String path) {
		if (this.contains(path) && this.isString(path)) return super.getString(path);
		if (this.save == null) return null;
		return this.save.getString(path);
	}
	
	@Override
	public List<Integer> getIntegerList(String path) {
		if (this.contains(path) && this.isList(path)) return super.getIntegerList(path);
		if (this.save == null) return null;
		return this.save.getIntegerList(path);
	}
	
	@Override
	public List<String> getStringList(String path) {
		if (this.contains(path) && this.isList(path)) return super.getStringList(path);
		if (this.save == null) return null;
		return this.save.getStringList(path);
	}
}